# README

* Online production version: [Click Here!](http://ec2-13-210-66-57.ap-southeast-2.compute.amazonaws.com:3333)

* Description
```
-Head to: problem_description.md
```

* Ruby version
```
Ruby 2.7.1
Rails >= 6.0.3
```

* System dependencies
```
yarn >= 1.22.10
```

* Configuration
```
$cd order_to_invoice_tdd/
$bundle install
$yarn install
$export RAILS_ENV=development
```

* Database creation
```
No DB. No ActiveRecord models.
Models are POROs.   
Data is in InvoiceModule::InvoicesData
```

* Run
```
rails s
```

* How to run the test suite
```
$cd order_to_invoice_tdd/
$rspec -fdoc

F: Creating an invoice
  Params inputs
    creates an invoice with valid inputs and show it
    creates an invoice with valid inputs, but no product packs
    creates an invoice with invalid inputs and show it

Invoice
  Data
    validates Invoices Data
  Model validations
    validates 1 valid invoice
    validates order_products values
    validates invoice total values
  Orders to invoice
    validates problem_description example
    validates multiple invoices. All with available packs
    validates multiple invoices. All with NO available packs
  Invoice to string
"10 WATERMELON $17.98\n" +
"  -2 X 5 packs @ $8.99\n" +
"14 PINEAPPLE $54.8\n" +
"  -3 X 2 packs @ $9.95  -1 X 8 packs @ $24.95\n" +
"13 ROCKMELON $25.85\n" +
"  -1 X 3 packs @ $5.95  -2 X 5 packs @ $9.95\n" +
"-------------------\n" +
"Total: 98.63"
    validates Invoice to_string text

Product
  Validations
    validates 1 valid product
    validates invalid values for amount
    validates invalid values for id
  Processing order to get best packs
    validates best_packs for one WATERMELON order
    validates product's order in the problem_description
    validates order where solution is not the bigger pack maximized
    validates WATERMELON order with no solution
    validates WATERMELON order less to all packs

ValidationModule::GuiParams
  Class methods
    validates valid inputs
    validates invalid inputs

ValidationModule::Numbers
  Class methods
    validates valid inputs
    validates invalid inputs

R: /invoices
  GET /invoices/new
    renders a successful response
  GET /invoices/show
    with valid parameters
      redirects to valid created invoice
    with invalid parameters
      redirects to the invalid invoice with errors

invoices/new
  renders View new, then validates invoice form

Finished in 0.35657 seconds (files took 4.13 seconds to load)
27 examples, 0 failures
```


