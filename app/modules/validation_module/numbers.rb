module ValidationModule
  module Numbers
    #@n Numeric. Number to validate
    #Any Not Numeric value will return False
    def self.posint_or_zero(n)
      if n.is_a?(Numeric)
        if n != 0 && !n.positive?
          return false
        end
        if !/\A\d+\z/.match(n.to_s)
          return false
        end
      else
        return false
      end
      return true
    end

    #@array Array<Numeric>. Array with Numbers to validate
    #Any not Array type for @array will return false
    #Any not Numeric value in @array will return False
    def self.array_posint_or_zero(array)
      if !array.is_a?(Array)
        return false
      end
      array.each do |n|
        if  !posint_or_zero(n)
          return false
        end
      end
      return true
    end
  end
end