module ValidationModule
  module GuiParams

    # @param Dict. i.e: {param1:'value1',WATERMELON:'3',PINEAPPLE:'5',ROCKMELON:'10'}
    # @return true if all product values are positive integer or zero.
    # false in otherwise.
    def self.product_params(params)
      if !params.is_a?(Hash) || params.empty?
        return false
      end
      #Extract product params.
      product_orders = [] #Array<String>
      InvoiceModule::InvoicesData::PRODUCT_DICT.values.each do |product_name|
        product_orders.append(params[product_name])
      end
      #validate each param extracted
      product_orders.each do |n|
        if n.is_a?(String)
          if !/\A\d+\z/.match(n)
            return false
          else
            if !ValidationModule::Numbers.posint_or_zero(n.to_i)
              return false
            end
          end
        else
          return false #when params[not_found_var] = nil
        end
      end
      return true
    end
  end
end