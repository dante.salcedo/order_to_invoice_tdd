module ProductModule
  module Products
    attr_accessor :amount,:id,:name
    attr_accessor :best_packs,:total
    attr_accessor :prod_prices,:prod_packs
    attr_accessor :pos_packs,:should_process,:found_solution #init in get_start_pos
    attr_accessor :details


    # @param [Integer] id: Product's Id
    # @param [Integer] amount: Order for this product
    def initialize(amount,id)
      @amount = amount
      @id = id
      @name = InvoiceModule::InvoicesData::PRODUCT_DICT[@id]
      @best_packs = []
      @prod_prices = InvoiceModule::InvoicesData::PRICES_ARRAY[@id]
      @prod_packs = InvoiceModule::InvoicesData::PACKS_ARRAY[@id]
      get_start_pos #which pack pos should to start checking
      set_packs_and_total #finds best packs according to prices
      @details = '' #details to print
    end

    # set [String] @details.
    # Add information about packs availability, best packs and price.
    def set_details
      if @best_packs == -1
        @details = I18n.t("msgs.no_packs",available_packs:@prod_packs.to_s)
      else
        length = @best_packs.length - 1
        (0..length).each do |i|
          if @best_packs[i] != 0
            aux = "  -"+@best_packs[i].to_s+" X "+@prod_packs[i].to_s+" packs @ $"+@prod_prices[i].to_s
            @details = @details + aux
          end
        end
      end
    end


    private
    # Identify the position to start checking the product's packs (@prod_packs).
    # If @amount = 5 and packs are: [2,5,8]. It'll start from 5 pos 1.
    def get_start_pos
      @found_solution = false #best packs according to prices
      @pos_packs = -1 #Start position in @prod_packs
      @should_process = false #Sometimes the @amount is less to all packs
      if self.valid? #validates @id and @amount
        #@pos_packs = @prod_packs.length - 1
        i = @prod_packs.length - 1
        while (i>=0)
          if  @amount >= @prod_packs[i]
            @pos_packs = i
            @should_process = true
            break
          end
          i-=1
        end
      end
    end

    # it finds best packs according to prices
    # Set values to @best_packs and @total
    # [Array<Integer>] @best_packs: Final solution. It ensures the best price
    # [Integer] @total: Value of @best_packs
    def set_packs_and_total
      if @should_process
        big_int = InvoiceModule::InvoicesData::BIGINT
        worst_packs = [big_int]*@prod_packs.length
        @best_packs = get_packs(@amount,worst_packs,worst_packs.clone,@pos_packs)
        if !@found_solution
          @best_packs = -1 #there is not pack combination that match with the order number '@amount'
          @total = 0
        else
          @total = InvoiceModule::Invoices.valorize_packs(@best_packs, @prod_prices)
          @total = @total.round(2)
        end
      else
        @best_packs = -1
        @total = 0
      end
    end

    # Recursive method to process the order of 1 product
    # @param [Integer] order: Order quantity.
    # @param [Array<Integer>] best_packs: Best solution until moment
    # @param [Array<Integer>] candidate: Possible solution to be the best
    # @param [Integer] pos_packs: Start position to check @prod_packs
    # @return [Array<Integer>] best_packs if there is Packs combination. Otherwise return -1
    def get_packs(order, best_packs, candidate,pos_packs)
      act_pack = @prod_packs[pos_packs]
      mod_order = order%act_pack
      if mod_order == 0 #Stop condition while checking the prodPacks
        candidate[pos_packs] = order/act_pack
        @found_solution = true
        if  InvoiceModule::Invoices.valorize_packs(candidate, @prod_prices) < InvoiceModule::Invoices.valorize_packs(best_packs, @prod_prices)
          candidate = InvoiceModule::Invoices.zerify(candidate) #Convert @BIGINT in 0
          best_packs = candidate #set candidate as Best
          return best_packs
        end
      else #Order can be reduced
        if pos_packs == 0
          return best_packs #Stop condition. It's in the first pack. Still no Solution.
        end
        if  order > act_pack
          candidate[pos_packs] = order/act_pack #Set possible amount in the candidate
        end
        pos_packs = pos_packs - 1 #To go back 1 pos in the prodPacks. New Cycle in the recursive method
        while pos_packs >= 0
          best_packs = get_packs(mod_order,best_packs,candidate,pos_packs)
          if !@found_solution
            #Some cycle reached the first pos and it did not find solution.
            if order == mod_order #Same that order<actual_pack.
              break #quit while. Have to go back.
            else
              if candidate[pos_packs+1] > 1
                candidate[pos_packs+1] -= 1
                mod_order = order - (candidate[pos_packs+1]*act_pack)
              else
                #With the actual candidate[posPacks+1] value there is not solution
                candidate[pos_packs+1] = InvoiceModule::InvoicesData::BIGINT #reset this value
                #In the prev pos was checked the order%act_pack
                mod_order = order #It needs to check the cycle´s order in the prev pos
              end
            end
          end
          if @found_solution
            return best_packs #It's works because bigger packs will always be founded first
          end
        end
        return best_packs
      end
    end
  end
end