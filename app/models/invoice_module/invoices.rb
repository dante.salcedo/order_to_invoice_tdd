module InvoiceModule
  module Invoices
    attr_accessor :order_products #Array<Integer>
    attr_accessor :products #Array<Product>
    attr_accessor :total #Float

    # @param [Array<Integer>] order_products. Order for each product
    def initialize(order_products)
      @order_products = order_products
      @products = []
      @total = 0
      init_invoice
    end

    # set details to all products included in this Invoice.
    # @param [Boolean] print_cl: true print the invoice in the command line.
    def prepare_to_print(print_cl)
      voucher = ""
      @products.each do | p |
        p.set_details
        voucher += "#{p.amount} #{p.name} $#{p.total}\n#{p.details}\n"
      end
      voucher += "-------------------\nTotal: #{@total}"
      if print_cl
        pp voucher
      end
    end

    # Calculate the total price using one packs array and one prices array.
    # @param [Array<Integer>] packs_array: i.e: [0,3,1]
    # @param [Array<Float>] prod_prices: i.e: [4.5,2.2,2.2]
    # @return [Float] total
    def self.valorize_packs(packs_array,prod_prices)
      total = 0
      length = prod_prices.length - 1
      (0..length).each do |i|
        aux = packs_array[i] * prod_prices[i]
        total = total + aux
      end
      return total
    end

    # Replace all BIGINT by 0 in the Array param
    # @param [Array<Integer>] candidate: i.e: [1,3,10000000]
    # @return [Array<Integer>] candidate: i.e: [1,3,0]
    def self.zerify(candidate)
      length = candidate.length - 1
      (0..length).each do |i|
        if candidate[i] == InvoiceModule::InvoicesData::BIGINT
          candidate[i] = 0
        end
      end
      return candidate
    end

    private
    # Calculate best_packs for each product.
    # Calculate invoice total
    def init_invoice
      length = InvoiceModule::InvoicesData.total_products - 1
      (0..length).each do |i| #process 1 product per cycle
        invoice_prod = Product.new(@order_products[i], i)
        @total += invoice_prod.total
        @products.append(invoice_prod)
      end
      @total = @total.round(2)
    end
    
  end
end