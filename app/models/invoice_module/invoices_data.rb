module InvoiceModule
  module InvoicesData

    class ProductName
      WATERMELON="WATERMELON"
      PINEAPPLE="PINEAPPLE"
      ROCKMELON="ROCKMELON"
    end

    #Simulate Data in DB
    PRODUCT_DICT = {0=>ProductName::WATERMELON,1=>ProductName::PINEAPPLE,2=>ProductName::ROCKMELON}
    #each element is an array with the available packs for each product respectively
    PACKS_ARRAY = [[3,5],[2,5,8],[3,5,9]]
    #each element is an array with the price for each Pack respectively
    PRICES_ARRAY = [[6.99,8.99],[9.95,16.95,24.95],[5.95,9.95,16.99]]
    #used in first loop of recursive algorithm
    BIGINT = 1000000

    def self.total_products
      return PRODUCT_DICT.length
    end

  end
end