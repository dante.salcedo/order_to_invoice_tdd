class Invoice
  include ActiveModel::Model
  include ActiveModel::Validations
  include InvoiceModule::Invoices

  validates_presence_of :order_products

  validates_each :order_products do |record, attr, value|
    record.errors.add attr, I18n.t("msgs.errors.products.no_posint_no_0") if !ValidationModule::Numbers.array_posint_or_zero(value)
  end

  validates :total, presence: true, numericality: {greater_than_or_equal_to: 0}

end