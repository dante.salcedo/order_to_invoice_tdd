class Product
  include ActiveModel::Validations
  include ProductModule::Products

  validates :amount, presence: true, numericality: { only_integer: true,greater_than_or_equal_to: 0}
  validates :id, presence: true, numericality: {
      only_integer: true,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: InvoiceModule::InvoicesData.total_products
  }


end