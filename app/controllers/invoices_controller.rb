class InvoicesController < ApplicationController

  def new
    @product_names = InvoiceModule::InvoicesData::PRODUCT_DICT.values
  end

  def show
    #params i.e = {"WATERMELON"=>"3", "PINEAPPLE"=>"5", "ROCKMELON"=>"10", "commit"=>"Generate Invoice"}
    product_names = InvoiceModule::InvoicesData::PRODUCT_DICT.values
    if ValidationModule::GuiParams.product_params(params.permit(product_names).to_h)
      product_orders = [] #string orders ["3","5","10"]
      product_names.each {|product_name| product_orders.append(params[product_name])}
      integer_orders = [] #integer orders [3,5,10]
      product_orders.each {|po| integer_orders.append(po.to_i)}
      @invoice = Invoice.new(integer_orders)
      @invoice.prepare_to_print(false)
      @error = false
    else
      logger.debug I18n.t('msgs.errors.no_posint_no_0')+". Check input params values or names"
      @info = I18n.t('msgs.errors.no_posint_no_0')
      @error = true
    end
  end
end
