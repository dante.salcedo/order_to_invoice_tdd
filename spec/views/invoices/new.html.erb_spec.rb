require 'rails_helper'

RSpec.describe "invoices/new", type: :view do
  before(:each) do
    @product_names = InvoiceModule::InvoicesData::PRODUCT_DICT.values
  end

  it "renders View new, then validates invoice form" do
    render
    assert_select "form[action=?][method=?]", invoices_show_path, "get" do
      #pp "products names: #{@product_names}"
      @product_names.each { |product_name| assert_select "input[name=?]", product_name}
    end
  end
end