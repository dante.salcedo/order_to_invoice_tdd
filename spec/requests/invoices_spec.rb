require 'rails_helper'

RSpec.describe "R: /invoices", type: :request do
  before do
    watermelon_name = InvoiceModule::InvoicesData::ProductName::WATERMELON #"WATERMELON"
    pineapple_name = InvoiceModule::InvoicesData::ProductName::PINEAPPLE #"PINEAPPLE"
    rockmelon_name = InvoiceModule::InvoicesData::ProductName::ROCKMELON #"ROCKMELON"
    @valid_attributes = {watermelon_name=>"3", pineapple_name=>"5", rockmelon_name=>"10", "commit"=>"Generate Invoice"}
    @invalid_attributes = {watermelon_name=>"-3", pineapple_name=>"5.1", rockmelon_name=>"A", "commit"=>"Generate Invoice"}
  end

  context "GET /invoices/new" do
    it "renders a successful response" do
      get invoices_new_url
      expect(response).to be_successful
    end
  end

  describe "GET /invoices/show" do
    context "with valid parameters" do
      it "redirects to valid created invoice" do
        get invoices_show_url, params: @valid_attributes
        expect(response).to be_successful
      end
    end
    context "with invalid parameters" do
      it "redirects to the invalid invoice with errors" do
        get invoices_show_url, params: @invalid_attributes
        expect(response).to be_successful
      end
    end
  end
end