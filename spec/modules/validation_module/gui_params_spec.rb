require 'rails_helper'

RSpec.describe ValidationModule::GuiParams do
  before do
    watermelon_name = InvoiceModule::InvoicesData::ProductName::WATERMELON #"WATERMELON"
    pineapple_name = InvoiceModule::InvoicesData::ProductName::PINEAPPLE #"PINEAPPLE"
    rockmelon_name = InvoiceModule::InvoicesData::ProductName::ROCKMELON #"ROCKMELON"
    #valid: All Product orders must be positive integer or zero.
    @valid_gui_params = {watermelon_name=>"3", pineapple_name=>"5", rockmelon_name=>"10", "commit"=>"Generate Invoice"}
    @invalid_gui_params1 = {watermelon_name=>"A", pineapple_name=>"-1", rockmelon_name=>"3.3", "commit"=>"Generate Invoice"}
    @invalid_gui_params2 = {  WATERMELON: '3', PINEAPPLE:'5', ROCKMELON:'10',commit:"Generate Invoice" }
  end

  describe "Class methods" do
    it "validates valid inputs" do
      expect(ValidationModule::GuiParams.product_params(@valid_gui_params)).to eq(true)
    end

    it "validates invalid inputs" do
      expect(ValidationModule::GuiParams.product_params(@invalid_gui_params1)).to eq(false)
      expect(ValidationModule::GuiParams.product_params(@invalid_gui_params2)).to eq(false)
    end
  end

end