require 'rails_helper'
RSpec.describe ValidationModule::Numbers do

  describe "Class methods" do
    it "validates valid inputs" do
      expect(ValidationModule::Numbers.posint_or_zero(3)).to eq(true)
      expect(ValidationModule::Numbers.posint_or_zero(0)).to eq(true)
      expect(ValidationModule::Numbers.array_posint_or_zero([0,1,2,1000])).to eq(true)
    end

    it "validates invalid inputs" do
      expect(ValidationModule::Numbers.posint_or_zero(-1)).to eq(false)
      expect(ValidationModule::Numbers.posint_or_zero(2.3)).to eq(false)
      expect(ValidationModule::Numbers.posint_or_zero("0")).to eq(false)

      expect(ValidationModule::Numbers.array_posint_or_zero(3)).to eq(false)
      expect(ValidationModule::Numbers.array_posint_or_zero([0,1,"a",1000])).to eq(false)
    end
  end

end