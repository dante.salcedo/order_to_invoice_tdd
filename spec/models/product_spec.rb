require 'rails_helper'
RSpec.describe Product, type: :model do
  before do
    watermelon_name = InvoiceModule::InvoicesData::ProductName::WATERMELON #"WATERMELON"
    pineapple_name = InvoiceModule::InvoicesData::ProductName::PINEAPPLE #"PINEAPPLE"
    rockmelon_name = InvoiceModule::InvoicesData::ProductName::ROCKMELON #"ROCKMELON"
    @valid_product = Product.new( 5, 1)
    @WATERMELON_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(watermelon_name)
    @PINEAPPLE_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(pineapple_name)
    @ROCKMELON_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(rockmelon_name)
  end

  context "Validations" do
    it "validates 1 valid product" do
      product = @valid_product
      product.valid?
      expect(product.errors[:amount]).to eq([])
      expect(product.errors[:id]).to eq([])
    end

    it "validates invalid values for amount" do
      product = Product.new(1.2, 1)
      product.valid?
      expect(product.errors[:amount]).to include(I18n.t('errors.messages.not_an_integer'))
      product.amount=''
      product.valid?
      expect(product.errors[:amount]).to include(I18n.t('errors.messages.blank'))
      product.amount = -1
      product.valid?
      expect(product.errors[:amount]).to include(I18n.t('errors.messages.greater_than_or_equal_to',count:0))
    end

    it "validates invalid values for id" do
      product = Product.new(1, 10.2)
      product.valid?
      expect(product.errors[:id]).to include(I18n.t('errors.messages.not_an_integer'))
      product.id = ''
      product.valid?
      expect(product.errors[:id]).to include(I18n.t('errors.messages.blank'))
      product.id = -1
      product.valid?
      expect(product.errors[:id]).to include(I18n.t('errors.messages.greater_than_or_equal_to',count:0))
      product.id = 1000
      product.valid?
      expect(product.errors[:id]).to include(I18n.t('errors.messages.less_than_or_equal_to',count:InvoiceModule::InvoicesData.total_products))
    end

  end

  context "Processing order to get best packs" do
    it "validates best_packs for one WATERMELON order" do
      # byebug
      product = Product.new(5, @WATERMELON_ID)
      pos_packs = 1 #in [3,5] should start from pos 1
      total = InvoiceModule::InvoicesData::PRICES_ARRAY[@WATERMELON_ID][pos_packs]
      expect(product.pos_packs).to eq(pos_packs)
      expect(product.should_process).to eq(true)
      expect(product.best_packs).to eq([0,1])
      expect(product.total).to eq(total)
    end

    #Orders Example included in the "Task Description"
    it "validates product's order in the problem_description" do
      product = Product.new(10, @WATERMELON_ID)
      expect(product.best_packs).to eq([0,2])
      expect(product.total).to eq(17.98)
      product = Product.new(14, @PINEAPPLE_ID)
      expect(product.best_packs).to eq([3,0,1])
      expect(product.total).to eq(54.80)
      product = Product.new(13, @ROCKMELON_ID)
      expect(product.best_packs).to eq([1,2,0])
      expect(product.total).to eq(25.85)
    end

    #-------EDGE CASES--------
    #div between order and bigger pack is not the best
    # 14W =slt> [3,1]. =packs> [3,5]. 14(order)/5(big pack) = 2. 2 isn't the best. 1 is best.
    # 25P =slt> [2,1,2]. =packs> [2,5,8]. 25(order)/8(big pack) = 3. 3 is not the best. Its 2.
    it "validates order where solution is not the bigger pack maximized" do
      product = Product.new(14, @WATERMELON_ID)
      expect(product.best_packs).to eq([3,1])
      product = Product.new(25, @PINEAPPLE_ID)
      expect(product.best_packs).to eq([2,1,2])
    end

    it "validates WATERMELON order with no solution" do
      product = Product.new(4, @WATERMELON_ID) #packs [3,5]
      pos_packs = 0 #useless to check 5 in [3,5]
      expect(product.pos_packs).to eq(pos_packs)
      expect(product.should_process).to eq(true)
      expect(product.best_packs).to eq(-1)
      expect(product.total).to eq(0)
    end

    it "validates WATERMELON order less to all packs" do
      product = Product.new(2, @WATERMELON_ID) #packs [3,5]
      expect(product.pos_packs).to eq(-1)
      expect(product.should_process).to eq(false)
    end
  end
end