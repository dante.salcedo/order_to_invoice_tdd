require 'rails_helper'
RSpec.describe Invoice, type: :model do
  before do
    watermelon_name = InvoiceModule::InvoicesData::ProductName::WATERMELON #"WATERMELON"
    pineapple_name = InvoiceModule::InvoicesData::ProductName::PINEAPPLE #"PINEAPPLE"
    rockmelon_name = InvoiceModule::InvoicesData::ProductName::ROCKMELON #"ROCKMELON"
    @valid_invoice = Invoice.new([1,2,3]) #total #15.89
    @TOTAL_PRODUCTS = InvoiceModule::InvoicesData.total_products
    @WATERMELON_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(watermelon_name)
    @PINEAPPLE_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(pineapple_name)
    @ROCKMELON_ID = InvoiceModule::InvoicesData::PRODUCT_DICT.key(rockmelon_name)
  end

  context "Data" do
    it "validates Invoices Data" do
      prices = InvoiceModule::InvoicesData::PRICES_ARRAY
      expect(prices.length).to eq(@TOTAL_PRODUCTS)
      packs = InvoiceModule::InvoicesData::PACKS_ARRAY
      expect(packs.length).to eq(@TOTAL_PRODUCTS)
      products = InvoiceModule::InvoicesData::PRODUCT_DICT
      expect(products.length).to eq(@TOTAL_PRODUCTS)
    end
  end

  context "Model validations" do
    it "validates 1 valid invoice" do
      invoice = @valid_invoice
      expect(invoice.valid?).to eq(true)
      expect(invoice.errors[:order_products]).to eq([])
      expect(invoice.errors[:products]).to eq([])
      expect(invoice.errors[:total]).to eq([])
    end

    it "validates order_products values" do
      invoice = Invoice.new('')
      invoice.valid?
      expect(invoice.errors[:order_products]).to include(I18n.t('errors.messages.blank'))
      invoice.order_products = [0,2,-3]
      invoice.valid?
      expect(invoice.errors[:order_products]).to include(I18n.t("msgs.errors.products.no_posint_no_0"))
      invoice.order_products = [0,'a',-3]
      invoice.valid?
      expect(invoice.errors[:order_products]).to include(I18n.t("msgs.errors.products.no_posint_no_0"))
    end

    it "validates invoice total values" do
      invoice = @valid_invoice
      invoice.total = ''
      invoice.valid?
      expect(invoice.errors[:total]).to include(I18n.t('errors.messages.blank'))
      invoice.total = -1
      invoice.valid?
      expect(invoice.errors[:total]).to include(I18n.t('errors.messages.greater_than_or_equal_to',count:0))
      invoice.total = 'string'
      invoice.valid?
      expect(invoice.errors[:total]).to include(I18n.t('errors.messages.not_a_number'))
    end

  end

  context "Orders to invoice" do
    it "validates problem_description example" do
      order_products = [10,14,13]
      invoice = Invoice.new(order_products)
      expect(invoice.valid?).to eq(true)
      expect(invoice.products.empty?).to eq(false)
      expect(invoice.products.length).to eq(InvoiceModule::InvoicesData.total_products)
      expect(invoice.total).not_to eq(0)

      product_1 = invoice.products[@WATERMELON_ID]
      expect(product_1.best_packs).to eq([0,2])
      expect(product_1.total).to eq(17.98)
      product_2 = invoice.products[@PINEAPPLE_ID]
      expect(product_2.best_packs).to eq([3,0,1])
      expect(product_2.total).to eq(54.80)
      product_3 = invoice.products[@ROCKMELON_ID]
      expect(product_3.best_packs).to eq([1,2,0])
      expect(product_3.total).to eq(25.85)

      expect(invoice.total).to eq(98.63)

    end

    it "validates multiple invoices. All with available packs " do
      orders = [[10,14,13],[10,5,13],[3,5,10],[3,5,9],[100,1231,12312],[10,4,6],[6,14,13]]
      orders.each do |o|
        invoice = nil
        invoice = Invoice.new(o)
        expect(invoice).not_to eq(nil)
        expect(invoice.valid?).to eq(true) #validates total and order_products
        expect(invoice.products).not_to be_empty
        expect(invoice.products.length).to eq(@TOTAL_PRODUCTS)
        invoice.products.each do |ip|
          expect(ip.is_a?(Product)).to eq(true)
          expect(ip.best_packs).not_to eq(-1) # -1 is product with no solution
          expect(ip.best_packs).not_to eq([]) #product not init
        end
      end
    end

    it "validates multiple invoices. All with NO available packs" do
      orders = [[4,3,2],[2,1,4],[1,1,1],[4,3,7]]
      orders.each do |o|
        invoice = nil
        invoice = Invoice.new(o)
        expect(invoice).not_to eq(nil)
        expect(invoice.valid?).to eq(true) #validates total and order_products
        expect(invoice.products).not_to be_empty
        expect(invoice.products.length).to eq(@TOTAL_PRODUCTS)
        invoice.products.each do |ip|
          expect(ip.is_a?(Product)).to eq(true)
          expect(ip.best_packs).to eq(-1) #product with no solution
        end
      end
    end
  end

  context "Invoice to string" do
    it "validates Invoice to_string text" do
      order_products = [10,14,13]
      invoice = Invoice.new(order_products)
      invoice.prepare_to_print(true)
      invoice.products.each do |ip|
        expect(ip.is_a?(Product)).to eq(true)
        expect(ip.details).not_to be_empty #product with no solution
      end
    end
  end

end