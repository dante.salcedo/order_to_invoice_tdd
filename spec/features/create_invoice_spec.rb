require 'rails_helper'

RSpec.describe 'F: Creating an invoice', type: :feature do
  before do
    @watermelon_name = InvoiceModule::InvoicesData::ProductName::WATERMELON #"WATERMELON"
    @pineapple_name = InvoiceModule::InvoicesData::ProductName::PINEAPPLE #"PINEAPPLE"
    @rockmelon_name = InvoiceModule::InvoicesData::ProductName::ROCKMELON #"ROCKMELON"
  end

  context "Params inputs" do
    it "creates an invoice with valid inputs and show it" do
      visit invoices_new_path
      fill_in @watermelon_name, with: '3'
      fill_in @pineapple_name, with: '5'
      fill_in @rockmelon_name, with: '10'
      click_on 'Generate Invoice'
      expect(page).to have_content('Receipt')
      expect(page).not_to have_content('Error')
    end

    it "creates an invoice with valid inputs, but no product packs" do
      visit invoices_new_path
      fill_in @watermelon_name, with: '4' #[3,5]
      fill_in @pineapple_name, with: '5'
      fill_in @rockmelon_name, with: '10'
      click_on 'Generate Invoice'
      expect(page).to have_content('Receipt')
      expect(page).not_to have_content('Error')
      expect(page).to have_content(I18n.t("msgs.no_packs",available_packs:[3,5]))
    end

    it "creates an invoice with invalid inputs and show it" do
      visit invoices_new_path
      fill_in @watermelon_name, with: '-3'
      fill_in @pineapple_name, with: '5.3'
      fill_in @rockmelon_name, with: 'a'
      click_on 'Generate Invoice'
      expect(page).to have_content('Error')
      expect(page).to have_content(I18n.t('msgs.errors.no_posint_no_0'))
      expect(page).not_to have_content('Receipt')
    end
  end

end