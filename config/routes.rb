Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get "/invoices/new", to: "invoices#new", as: "invoices_new"
  get "/invoices/show", to: "invoices#show", as: "invoices_show"
  root "invoices#new"
end
